
var shell = WScript.CreateObject('WScript.Shell');

var dirname = WScript.Arguments.Item(0);

var username = WScript.Arguments.Item(1);
var password = WScript.Arguments.Item(2);

shell.Run('C:\\Windows\\System32\\runas.exe /user:' + username + ' cmd', 2);

WScript.Sleep(50);

shell.SendKeys(password);
shell.SendKeys('{ENTER}');

WScript.Sleep(50);

shell.SendKeys('md ' + dirname);
shell.SendKeys('{ENTER}');

WScript.Sleep(100);
