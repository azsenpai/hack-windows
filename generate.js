
var shell = WScript.CreateObject('WScript.Shell');
var FSO = WScript.CreateObject('Scripting.FileSystemObject');

var characters = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'];
var l = 4;

if (WScript.Arguments.length > 0) {
    l = parseInt(WScript.Arguments.Item(0));
}

var dirname = 'C:\\try-' + (new Date()).getTime();

var username = 'Admin';
var password = new Array(l);

var wscript_bin = 'C:\\Windows\\System32\\wscript.exe';

rec(0, l);

function rec(i, l) {
    if (i == l) {
        var command = wscript_bin + ' try.js ' + dirname + ' ' + username + ' ' + password.join('');
        WScript.Echo(command);

        shell.Run(command, 1, true);

        if (FSO.FolderExists(dirname)) {
            WScript.Echo('\n');
            WScript.Echo('Password: ' + password.join(''));
            WScript.Echo('\n');

            WScript.Quit();
        }
    } else {
        for (var j = 0; j < characters.length; j ++) {
            password[i] = characters[j];
            rec(i+1, l);
        }
    }
}
